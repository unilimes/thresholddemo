module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "no-undef": "off",
        "no-console": "off",
        "no-unused-vars": "off",
        "indent": ["error", 4],
        "linebreak-style": ["off", "unix"],
        "quotes": ["error", "single"],
        "semi": ["error", "always"]
    }
};
