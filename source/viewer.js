import { Engine } from './engine/engine';
import { Utils } from './engine/utils';
import { Pano } from './panorama/pano';

let engine = null;
let scene = null;

export class Viewer {
    constructor(container) {

        if (container && engine === null) {
            engine = new Engine( container );
            scene = new THREE.Group();
            scene.name = 'Hierarchy';
            engine.scene.add(scene);
        }

        this.sceneAlpha = 1;

        this.activePanorama = null;

        this.engine = engine;
        this.scene = scene;

        this.camera = engine.camera;
        this.camera._frustum = new THREE.Frustum();
        this.camera._frustumUpdate = function() {
            this._frustum.setFromMatrix(
                new THREE.Matrix4().multiplyMatrices(
                    this.projectionMatrix,
                    this.matrixWorldInverse
                )
            );
        };
        this.camera.position.fromArray( [ -0.009448, 0.001920, 0.002653] );
        this.camera.fov = 75;
        this.camera.updateProjectionMatrix();

        this.scene.add(this.camera);

        this.cameraP = engine.cameraP;
        this.cameraO = engine.cameraO;
        this.controls = engine.controls;
        this.transform = engine.transform;
        this.renderer = engine.renderer;
        this.raycaster = engine.raycaster;

        this.width = engine.width;
        this.height = engine.height;

        this.listeners();
        this.init();

    }

    listeners() {

        let rotateOn = false;

        this.engine.on('resize', (event) => {
            this.panorama.setOnCamera( this.camera );
        });

        this.engine.on('rotstart', (event) => {
            rotateOn = true;
        });

        this.engine.on('rotend', (event) => {
            rotateOn = false;
        });

        this.engine.on('rotmove', (event) => {
            if(rotateOn) {
                this.panorama.setOnCamera( this.camera );
            }
        });

        $('#fade-button').click(() => {

            this.fadeInOut();

        });

        let hammertime = new Hammer( document.getElementById('renderer') );
        hammertime.get('pinch').set({ enable: true });
        hammertime.on('pinch', (ev) => {
            let i = 1;
            if(ev.scale  > 1.0) {
                i = -1;
            }
            this.camera.fov = this.camera.fov + (i);
            this.camera.updateProjectionMatrix();
        });

        this.engine.on('mousewheel', (event) => {
            let i = 1;
            if( event.deltaY < 0 ) {
                i = -1;
            }
            let nextFov = this.camera.fov + (2*i);
            this.camera.fov =   ( nextFov < 25 ) ? 25 : ( nextFov > 125 ) ? 125 : nextFov;
            this.camera.updateProjectionMatrix();
        });

    }

    init() {

        let data = {
            filename: '2',
            type: 'jpg',
            path: 'dist/images/2/'
        };

        let geoSphere = new THREE.SphereBufferGeometry(50,64,64);

        let matSphere = new THREE.MeshBasicMaterial({
            side: THREE.FrontSide,
            color: 0xff0000,
            depthTest: false,
            depthWrite: false
        });

        let sph = new THREE.Mesh(
            geoSphere,
            matSphere
        );

        sph.position.x = 175;

        this.scene.add(sph);

        this.panorama = new Pano( data, () => {

            console.log( this.panorama );

            this.activePanorama = this.panorama;

            this.scene.add( this.panorama.inner );

            setTimeout( () => {
                this.panorama.setOnCamera( this.camera );
            }, 500 );

        } );

    }

    fadeInOut() {

        let starta = {x: this.sceneAlpha};

        this.sceneAlpha = 1 - this.sceneAlpha;

        let enda = {x: this.sceneAlpha};

        let delay = 2000;

        let fade = new TWEEN.Tween( starta ).to( enda, delay ).easing( TWEEN.Easing.Linear.None )
            .onStart(() => {})
            .onUpdate((a) => {

                this.activePanorama.setAlpha(starta.x);

            })
            .onComplete(() => {});

        fade.start();

    }

    setDoorAlpha(alpha) {
        this.activePanorama.setDoorAlpha(alpha);
    }

}