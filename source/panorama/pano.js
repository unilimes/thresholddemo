export class Pano {

    constructor( data, onLoad ) {

        this.data = data || {};

        this.sections = 8;
        this.steps = 8;

        this.baseSize = 48;

        this.activeOpacity = 1;
        this.activeDoorOpacity = 0.5;

        this.piLength = (Math.PI * 2)/this.sections;
        this.thetaLength = Math.PI/this.sections;

        //
        this.images = {};

        for(let i = 0; i < this.sections; i++) {
            this.images[i] = [];
        }
        //

        this._onLoad = function(){};

        this.inner = new THREE.Group();

        this.inner.name = 'Inner';

        this.rotx = 0.0;

        this.inner.rotation.x = this.rotx;

        this.material = new THREE.MeshBasicMaterial({
            side: THREE.DoubleSide
        });

        
        let imagesToLoad = 0;


        for(let i = 0; i < this.sections; i++) {

            let piStart = i * this.piLength;

            for(let j = 0; j < this.steps; j++) {

                imagesToLoad++;

                let thetaStart = j * this.thetaLength;

                let section = new THREE.Mesh(
                    new THREE.SphereBufferGeometry(
                        100,
                        this.baseSize/this.sections,
                        this.baseSize/this.steps,
                        piStart,
                        this.piLength,
                        thetaStart,
                        this.thetaLength
                    )
                );

                section.orderIndex = i;
                section.stepIndex = j;
                section.geometry.scale(-1,1,1);

                this.inner.add(section);

            }

        }

        this.onLoad( onLoad );

        this.loadShader();

    }

    onLoad( func ){
        if( typeof func == 'function' ){
            this._onLoad = func;
        }
    }

    setOnCamera( camera ) {

        camera._frustumUpdate();

        this.inner.children.forEach( (child) => {

            let updateTextureQuality = () => {

                let src = this.data.path + this.data.filename + '-' + child.stepIndex + '-' + child.orderIndex + '.jpg';

                new THREE.TextureLoader().load( src, ( txtr ) => {

                    let alpha = this.data.path + 'alpha/' + this.data.filename + '-' + child.stepIndex + '-' + child.orderIndex + '.jpg';

                    new THREE.TextureLoader().load( alpha, ( am ) => {
                        let mat = this.shaderHighRes.clone();

                        mat.uniforms.map.value = txtr;
                        mat.uniforms.map.value.needsUpdate = true;

                        mat.uniforms.doorIntensity.value = this.activeDoorOpacity;
                        mat.uniforms.intensity.value = this.activeOpacity;

                        child.material = mat;

                    });


                } );

                child.done = true;
            };

            if( !child.done && camera._frustum.intersectsObject( child ) ){
                updateTextureQuality();
            }

        });

    }

    highRes() {

        this.shaderHighRes = new THREE.ShaderMaterial({
            uniforms: {
                map: {
                    type: 't', value: null
                },
                intensity: {
                    type: 'f', value: 1.0
                },
                doorIntensity: {
                    type: 'f', value: this.activeDoorOpacity
                },
                point_l: {
                    type: 'v3', value: new THREE.Vector3
                },
                point_o: {
                    type: 'v3', value: new THREE.Vector3
                },
                plane: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                plane1: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                plane2: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                plane3: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                plane4: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                rotationY: {
                    type: 'f', value: 0
                },
                rotationX: {
                    type: 'f', value: 0
                },
                placement: {
                    type: 'v3', value: new THREE.Vector3
                }
            },
            vertexShader: '\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							void main () {\n\
								vec4 p = vec4 (position, 1.0);\n\
								vUv = uv;\n\
								worldPosition = (modelMatrix * p).xyz;\n\
								gl_Position = projectionMatrix * modelViewMatrix * p;\n\
								//gl_Position = vec4(uv * 2.0 - vec2(1.0), 0.0, 1.0);\n\
							}',
            fragmentShader: '\
							uniform sampler2D map;\n\
							uniform vec3 placement;\n\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							uniform vec3 point_l;\n\
							uniform vec3 point_o;\n\
							uniform vec4 plane;\n\
							uniform vec4 plane1;\n\
							uniform vec4 plane2;\n\
							uniform vec4 plane3;\n\
							uniform vec4 plane4;\n\
							uniform float radius;\n\
							uniform float intensity;\n\
							uniform float doorIntensity;\n\
							uniform float rotationY;\n\
							uniform float rotationX;\n\
							uniform sampler2D alpha;\n\
							void main () {\n\
								vec3 R = worldPosition - placement;\n\
								float oldY = R.y;\n\
								float oldZ = R.z;\n\
								R.y = oldY * cos(rotationX) - oldZ * sin(rotationX);\n\
                                R.z = oldY * sin(rotationX) + oldZ * cos(rotationX);\n\
                                float pp = 1.0;\n\
								float r = length (R);\n\
								float theta = acos(-R.y / r);\n\
								float phi = atan(R.x, -R.z);\n\
								float roty =  (0.75 + phi  / 6.2831853072) + ( rotationY  / 6.2831853072);\n\
								vec3 normR = normalize(R);\n\
								vec3 normL = normalize(point_l);\n\
								if(distance(normR, normL) < 1.414) {\n\
                                    float t = -(plane.w)/(plane.x*R.x + plane.y*R.y + plane.z*R.z);\n\
                                    if(t != 0.0) {\n\
                                        float xo = R.x*t;\n\
                                        float yo = R.y*t;\n\
                                        float zo = R.z*t;\n\
                                        vec3 P = vec3(xo,yo,zo);\n\
                                        if(dot((P - point_o),vec3(plane3.x, plane3.y, plane3.z)) > 0.0 && dot((P - point_l),vec3(plane2.x, plane2.y, plane2.z)) > 0.0 && dot((P - point_l),vec3(plane1.x, plane1.y, plane1.z)) < 0.0 && dot((P - point_o),vec3(plane4.x, plane4.y, plane4.z)) < 0.0){\n\
                                            pp = doorIntensity;\n\
                                        }\n\
                                    }\n\
                                }\n\
                                if(roty > 1.0) {\n\
                                    roty = roty - 1.0;\n\
                                }\n\
                                if(roty < .0) {\n\
                                    roty = roty + 1.0;\n\
                                }\n\
                                vec4 test = texture2D(map, vUv);\n\
								gl_FragColor = vec4(vec3(test),pp * intensity);\n\
								\n\
							}'
        });


        this.shaderHighRes.side = THREE.FrontSide;
        this.shaderHighRes.transparent = true;

    }

    loadShader() {

        this.highRes();

        this.shader = new THREE.ShaderMaterial({
            uniforms: {
                map: {
                    type: 't', value: null
                },
                alpha: {
                    type: 't', value: null
                },
                intensity: {
                    type: 'f', value: 1.0
                },
                doorIntensity: {
                    type: 'f', value: this.activeDoorOpacity
                },
                point_l: {
                    type: 'v3', value: new THREE.Vector3
                },
                point_o: {
                    type: 'v3', value: new THREE.Vector3
                },
                plane: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                plane1: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                plane2: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                plane3: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                plane4: {
                    type: 'v4', value: new THREE.Vector4(1.0,1.0,1.0,0.0)
                },
                rotationY: {
                    type: 'f', value: 0
                },
                rotationX: {
                    type: 'f', value: 0
                },
                placement: {
                    type: 'v3', value: new THREE.Vector3
                }
            },
            vertexShader: '\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							void main () {\n\
								vec4 p = vec4 (position, 1.0);\n\
								worldPosition = (modelMatrix * p).xyz;\n\
								gl_Position = projectionMatrix * modelViewMatrix * p;\n\
								//gl_Position = vec4(uv * 2.0 - vec2(1.0), 0.0, 1.0);\n\
							}',
            fragmentShader: '\
							uniform sampler2D map;\n\
							uniform vec3 placement;\n\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							uniform vec3 point_l;\n\
							uniform vec3 point_o;\n\
							uniform vec4 plane;\n\
							uniform vec4 plane1;\n\
							uniform vec4 plane2;\n\
							uniform vec4 plane3;\n\
							uniform vec4 plane4;\n\
							uniform float radius;\n\
							uniform float intensity;\n\
							uniform float doorIntensity;\n\
							uniform float rotationY;\n\
							uniform float rotationX;\n\
							uniform sampler2D alpha;\n\
							void main () {\n\
								vec3 R = worldPosition - placement;\n\
								float oldY = R.y;\n\
								float oldZ = R.z;\n\
								R.y = oldY * cos(rotationX) - oldZ * sin(rotationX);\n\
                                R.z = oldY * sin(rotationX) + oldZ * cos(rotationX);\n\
                                float pp = 1.0;\n\
								float r = length (R);\n\
								float theta = acos(-R.y / r);\n\
								float phi = atan(R.x, -R.z);\n\
								float roty =  (0.75 + phi  / 6.2831853072) + ( rotationY  / 6.2831853072);\n\
                                vec3 normR = normalize(R);\n\
								vec3 normL = normalize(point_l);\n\
								if(distance(normR, normL) < 1.414) {\n\
                                    float t = -(plane.w)/(plane.x*R.x + plane.y*R.y + plane.z*R.z);\n\
                                    if(t != 0.0) {\n\
                                        float xo = R.x*t;\n\
                                        float yo = R.y*t;\n\
                                        float zo = R.z*t;\n\
                                        vec3 P = vec3(xo,yo,zo);\n\
                                        if(dot((P - point_o),vec3(plane3.x, plane3.y, plane3.z)) > 0.0 && dot((P - point_l),vec3(plane2.x, plane2.y, plane2.z)) > 0.0 && dot((P - point_l),vec3(plane1.x, plane1.y, plane1.z)) < 0.0 && dot((P - point_o),vec3(plane4.x, plane4.y, plane4.z)) < 0.0){\n\
                                            pp = doorIntensity;\n\
                                        }\n\
                                    }\n\
                                }\n\
                                if(roty > 1.0) {\n\
                                    roty = roty - 1.0;\n\
                                }\n\
                                if(roty < .0) {\n\
                                    roty = roty + 1.0;\n\
                                }\n\
                                vec4 test = texture2D(map, vec2 (\n\
                                    roty,\n\
                                    theta / 3.1415926536\n\
                                ));\n\
                                vec4 am = texture2D(alpha, vec2 (\n\
                                    roty,\n\
                                    theta / 3.1415926536\n\
                                ));\n\
								gl_FragColor = vec4(vec3(test),pp * intensity);\n\
								\n\
							}'
        });


        let top_l = new THREE.Vector3(25,  15, -25); //top left
        let top_r = new THREE.Vector3(25,  15,  5);  //top right
        let bot_l= new THREE.Vector3(25, -30, -25);  //bottom left
        let bot_r = new THREE.Vector3(25, -30,  5);  //bottom right

        let plane = new THREE.Plane();

        plane.setFromCoplanarPoints(top_l,top_r,bot_l);

        let front_top_l = new THREE.Vector3(top_l.x + plane.normal.x, top_l.y + plane.normal.y, top_l.z + plane.normal.z);
        let front_top_r = new THREE.Vector3(top_r.x + plane.normal.x, top_r.y + plane.normal.y, top_r.z + plane.normal.z);
        let front_bot_r = new THREE.Vector3(bot_r.x + plane.normal.x, bot_r.y + plane.normal.y, bot_r.z + plane.normal.z);


        let plane1 = new THREE.Plane();
        plane1.setFromCoplanarPoints(top_l,top_r,front_top_l); //top
        let plane2 = new THREE.Plane();
        plane2.setFromCoplanarPoints(top_l,front_top_l,bot_l); //right
        plane2.normal.z *= -1;
        let plane3 = new THREE.Plane();
        plane3.setFromCoplanarPoints(bot_r,top_r,front_top_l); //left
        let plane4 = new THREE.Plane();
        plane4.setFromCoplanarPoints(bot_r,front_bot_r,bot_l); //bottom
        plane4.normal.y *= -1;

        this.shader.uniforms.plane.value  = new THREE.Vector4(plane.normal.x,  plane.normal.y,  plane.normal.z,  plane.constant);
        this.shader.uniforms.plane1.value = new THREE.Vector4(plane1.normal.x, plane1.normal.y, plane1.normal.z, plane1.constant);
        this.shader.uniforms.plane2.value = new THREE.Vector4(plane2.normal.x, plane2.normal.y, plane2.normal.z, plane2.constant);
        this.shader.uniforms.plane3.value = new THREE.Vector4(plane3.normal.x, plane3.normal.y, plane3.normal.z, plane3.constant);
        this.shader.uniforms.plane4.value = new THREE.Vector4(plane4.normal.x, plane4.normal.y, plane4.normal.z, plane4.constant);

        this.shader.uniforms.point_l.value = top_l;
        this.shader.uniforms.point_o.value = bot_r;

        this.shader.uniforms.rotationX.value = -this.rotx;

        this.shader.side = THREE.FrontSide;
        this.shader.transparent = true;


        //highres
        this.shaderHighRes.uniforms.plane.value  = new THREE.Vector4(plane.normal.x,  plane.normal.y,  plane.normal.z,  plane.constant);
        this.shaderHighRes.uniforms.plane1.value = new THREE.Vector4(plane1.normal.x, plane1.normal.y, plane1.normal.z, plane1.constant);
        this.shaderHighRes.uniforms.plane2.value = new THREE.Vector4(plane2.normal.x, plane2.normal.y, plane2.normal.z, plane2.constant);
        this.shaderHighRes.uniforms.plane3.value = new THREE.Vector4(plane3.normal.x, plane3.normal.y, plane3.normal.z, plane3.constant);
        this.shaderHighRes.uniforms.plane4.value = new THREE.Vector4(plane4.normal.x, plane4.normal.y, plane4.normal.z, plane4.constant);

        this.shaderHighRes.uniforms.point_l.value = top_l;
        this.shaderHighRes.uniforms.point_o.value = bot_r;

        this.shaderHighRes.uniforms.rotationX.value = -this.rotx;



        let image = new Image();

        image.onload = () => {

            let texture = new THREE.Texture(image);

            texture.magFilter = THREE.LinearFilter;
            texture.minFilter = THREE.LinearFilter;

            this.shader.uniforms.map.value = texture;
            this.shader.uniforms.map.value.needsUpdate = true;
            this.shader.uniforms.placement.value = {x:0, y:0, z:0};

            let map = new Image();

            map.onload = () => {

                let amap = new THREE.Texture(map);

                amap.magFilter = THREE.LinearFilter;
                amap.minFilter = THREE.LinearFilter;

                this.shader.uniforms.alpha.value = amap;
                this.shader.uniforms.alpha.value.needsUpdate = true;

                this.inner.children.forEach( (child) => {
                    child.material = this.shader;

                    if(child.stepIndex === 3 && child.orderIndex%2 === 0) {
                        child.done = true;
                    }
                });

                this._onLoad();

            };

            map.crossOrigin = 'anonymous';

            map.src = this.data.path + '/' + this.data.filename + '_alpha.jpg';

        };

        image.crossOrigin = 'anonymous';

        let src = this.data.path + '/vlowres/' + this.data.filename + '.jpg';

        image.src = src;
    }

    setAlpha(alpha) {

        this.activeOpacity = alpha;

        this.shader.uniforms.intensity.value = alpha;

        this.inner.children.forEach((child) => {
            if(child.done) {
                child.material.uniforms.intensity.value = alpha;
            }
        });

    }

    setDoorAlpha(alpha) {

        this.activeDoorOpacity = alpha;

        this.shader.uniforms.doorIntensity.value = alpha;

        this.inner.children.forEach((child) => {

            if(child.done) {
                child.material.uniforms.doorIntensity.value = alpha;
            }

        });

    }

}