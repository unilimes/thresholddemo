export class Utils {

    constructor() {

    }

    static getNormalBy3Points( points ){
        let tempGeometry = new THREE.Geometry();
        tempGeometry.vertices.push( points[0].clone() );
        tempGeometry.vertices.push( points[1].clone() );
        tempGeometry.vertices.push( points[2].clone() );
        var normal = new THREE.Vector3( 0, 1, 0 ); //optional
        var face = new THREE.Face3( 0, 1, 2, normal );
        tempGeometry.faces.push( face );
        tempGeometry.computeFaceNormals();
        return tempGeometry.faces[0].normal.clone();
    }
}
