export class Events {
    constructor() {
        this.delegate = {};
    }

    on(args, callback) {
        args.split(',').forEach((name) => {
            name = name.trim();

            if (this.delegate[name]) {
                this.delegate[name].push(callback);
            } else {
                this.delegate[name] = [callback];
            }
        });

        return this;
    }

    off(args) {
        args.split(',').forEach((name) => {
            name = name.trim();

            delete this.delegate[name];
        });

        return this;
    }

    emit(args, ...params) {
        args.split(',').forEach((name) => {
            name = name.trim();

            if (this.delegate[name]) {
                this.delegate[name].forEach((event) => {
                    event(...params);
                });
            }
        });

        return this;
    }
}
