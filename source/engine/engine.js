import { Events } from './events.js';
import { Raycaster } from './raycaster.js';

export class Engine extends Events {
    constructor(selector) {
        super();

        // Init

        this.container = document.querySelector( selector ) || document.body;

        this.width = this.container.offsetWidth;
        this.height = this.container.offsetHeight;

        // Scene
        this.scene = new THREE.Scene();
        this.scene.name = 'Scene';

        // Renderer
        this.renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });
        this.renderer.setClearColor( 0x000000, 1.0 );
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize( this.width, this.height );

        this.container.appendChild( this.renderer.domElement );

        // Camera
        this.cameraP = new THREE.PerspectiveCamera( 75, this.width / this.height, 0.001, 100000 );
        this.cameraP.name = 'CameraP';
        this.cameraP.position.set(0, 1, 1.4);

        this.cameraO = new THREE.OrthographicCamera( this.width / - 2, this.width / 2, this.height / 2, this.height / - 2, 1, 100000 );
        this.cameraO.name = 'CameraO';
        this.cameraO.position.set( 0, 50, 70 );

        this.camera = this.cameraP;

        // Controls
        this.controls = new THREE.OrbitControls( this.camera, this.renderer.domElement );
        this.controls.device = false;
        this.controls.inverse = true;
        this.controls.enablePan = false;
        this.controls.enableZoom = false;
        this.controls.enableDamping = false;
        this.controls.target.set(0, 0, 0);
        this.controls.update();

        this.everyFrameStack = [];

        // Raycaster
        this.raycaster = new Raycaster( this.camera, this.renderer );

        this.frame = 0;

        // Function
        this.listen();
        this.render();
        this.debug();

    }

    listen() {


        THREE.DefaultLoadingManager.onStart = () => this.emit('start');
        THREE.DefaultLoadingManager.onProgress = (item, loaded, total) => this.emit('progress', (loaded / total * 100), item, loaded, total);
        THREE.DefaultLoadingManager.onLoad = () => this.emit('load');
        THREE.DefaultLoadingManager.onError = () => this.emit('error');


        this.renderer.domElement.addEventListener('click', (event) => this.emit('click', event), false);
        this.renderer.domElement.addEventListener('dblclick', (event) => this.emit('dblclick', event), false);
        this.renderer.domElement.addEventListener('contextmenu', (event) => this.emit('contextmenu', event), false);


        this.renderer.domElement.addEventListener( 'mousewheel', (event) => this.emit('mousewheel', event), false);


        this.renderer.domElement.addEventListener('mouseup', (event) => this.emit('mouseup, pointerup, rotend', event), false);
        this.renderer.domElement.addEventListener('touchend', (event) => this.emit('rotend', event), false);


        this.renderer.domElement.addEventListener('gesturestart', (event) => this.emit('gesturestart', event), false);
        this.renderer.domElement.addEventListener('gesturechange', (event) => this.emit('gesturechange', event), false);
        this.renderer.domElement.addEventListener('gestureend', (event) => this.emit('gestureend', event), false);


        this.renderer.domElement.addEventListener('mousedown', (event) => this.emit('mousedown, pointerdown, rotstart', event), false);
        this.renderer.domElement.addEventListener('touchstart', (event) => this.emit('rotstart', event), false);

        this.renderer.domElement.addEventListener('mousemove', (event) => this.emit('mousemove, pointermove, rotmove', event), false);

        this.renderer.domElement.addEventListener('mousewheel', (event) => this.emit('mousewheel', event), false);

        this.renderer.domElement.addEventListener('touchmove', (event) => this.emit('rotmove', event), false);

        window.addEventListener('keydown', (event) => this.emit('keydown', event), false);
        window.addEventListener('keyup', (event) => this.emit('keyup', event), false);

        window.addEventListener('orientationchange', () => {
            this.resize();
            this.emit('orientationchange');
        }, false);

        window.addEventListener( 'resize', () => {
            this.resize();
            this.emit('resize');
        }, false);

    }

    resize(width, height) {
        this.width = width || window.innerWidth;
        this.height = height || window.innerHeight;

        this.renderer.setPixelRatio(window.devicePixelRatio);

        this.camera.aspect = this.width / this.height;
        this.camera.updateProjectionMatrix();


        this.renderer.setSize(this.width, this.height);

    }

    render() {
        this.renderer.clear();

        this.renderer.render(this.scene, this.camera);

        this.frame++;
        for( let i = 0; i < this.everyFrameStack.length; i++ ){
            this.everyFrameStack[i]( this.frame );
        }

        window.requestAnimationFrame(() => {

            TWEEN.update();

            this.controls.update();

            this.render();
        });
    }

    everyFrame( callback ){
        if( callback ){
            this.everyFrameStack.push( callback );
        }
    }

    debug() {
        window.scene = this.scene;
        window.camera = this.camera;
        window.controls = this.controls;
        window.renderer = this.renderer;
    }
}
