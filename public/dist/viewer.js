/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
__webpack_require__(1);var _viewer=__webpack_require__(2);window.panoViewer=new _viewer.Viewer('#renderer');

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports,"__esModule",{value:true});exports.Viewer=undefined;var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();var _engine=__webpack_require__(3);var _utils=__webpack_require__(6);var _pano=__webpack_require__(7);var _door=__webpack_require__(8);function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}var engine=null;var scene=null;var Viewer=exports.Viewer=function(){function Viewer(container){_classCallCheck(this,Viewer);if(container&&engine===null){engine=new _engine.Engine(container);scene=new THREE.Group();scene.name='Hierarchy';engine.scene.add(scene);}var multiplyFactor=1.3;this.sceneAlpha=1;var doorPoints=[new THREE.Vector3(-(38*multiplyFactor/2),58*multiplyFactor/2,0),new THREE.Vector3(38*multiplyFactor/2,58*multiplyFactor/2,0),new THREE.Vector3(38*multiplyFactor/2,-(58*multiplyFactor/2),0),new THREE.Vector3(-(38*multiplyFactor/2),-(58*multiplyFactor/2),0)];this.activePanorama=null;// let door = new Door( doorPoints );
// door.position.copy( (new THREE.Vector3( 32, -10, -13 )).multiplyScalar( multiplyFactor ) );
// door.rotation.set( 0, -Math.PI/2, 0 );
//console.log( door );
// engine.scene.add( door.mesh );
// engine.everyFrame( ( currentFrame ) => {
//     door.animate( currentFrame );
// } );
// let ungeometry = new THREE.PlaneGeometry( (38*multiplyFactor*0.3), (58 * multiplyFactor * 0.3) );
// let unmaterial =   new THREE.MeshBasicMaterial({
//     depthTest: true,
//     depthWrite: true,
//     color: 0xffffff,
//     opacity: 0.4,
//     transparent: true,
//     side: THREE.DoubleSide
// });
//
// unmaterial.blending = THREE.CustomBlending;
// unmaterial.blendEquation = THREE.AddEquation; //default
// unmaterial.blendSrc = THREE.SrcAlphaFactor; //default
// unmaterial.blendSrcAlpha = THREE.OneFactor; //default
// unmaterial.blendDst = THREE.DstColorFactor; //default
//
// let undefinedPlane = new THREE.Mesh( ungeometry, unmaterial );
// undefinedPlane.position.copy(  door.position.clone().multiplyScalar( 0.3 )  );
// undefinedPlane.rotation.set( 0, -Math.PI/2, 0 );
// engine.scene.add( undefinedPlane );
//
//console.log( undefinedPlane );
// let g1 = new THREE.SphereGeometry( 1, 5, 5 );
// let m1 = new THREE.MeshBasicMaterial({ color: 0xffffff });
// let sph = new THREE.Mesh( g1, m1 );
// sph.position.set( 15, 0, -2 );
// engine.scene.add( sph );
// console.log( sph );
// var verticalMirror = new THREE.Reflector( ungeometry.clone(), {
//     clipBias: 0.003,
//     textureWidth: 1024,
//     textureHeight: 1024,
//     color: 0x889999,
//     recursion: 1,
//     viewer: this
// } );
// //
// verticalMirror.position.set(  12, -4, -5  );
// verticalMirror.rotation.set( 0, -Math.PI/2, 0 );
// engine.scene.add( verticalMirror );
//
//console.log( verticalMirror );
//
//let nextNormal = Utils.getNormalBy3Points([
//    doorPoints[0],
//    doorPoints[1],
//    doorPoints[2]
//]);
//console.log( nextNormal );
//let plane = new THREE.Plane( nextNormal, 0 );
//let geom = new THREE.BoxGeometry( 1, 1, 1 );
//geom = sliceGeometry( geom, plane );
//let material = new THREE.MeshBasicMaterial({ wireframe: true });
//let mesh = new THREE.Mesh( geom, material );
//engine.scene.add(mesh);
//scene.rotation.y = Math.PI*Math.random()*0.3;
//scene.rotation.y = Math.PI/2;
this.engine=engine;this.scene=scene;this.camera=engine.camera;//console.log( this.camera );
this.camera._frustum=new THREE.Frustum();this.camera._frustumUpdate=function(){this._frustum.setFromMatrix(new THREE.Matrix4().multiplyMatrices(this.projectionMatrix,this.matrixWorldInverse));};this.camera.position.fromArray([-0.009448,0.001920,0.002653]);this.camera.fov=75;this.camera.updateProjectionMatrix();this.scene.add(this.camera);this.cameraP=engine.cameraP;this.cameraO=engine.cameraO;this.controls=engine.controls;this.transform=engine.transform;this.renderer=engine.renderer;this.raycaster=engine.raycaster;this.width=engine.width;this.height=engine.height;this.listeners();this.init();}_createClass(Viewer,[{key:'listeners',value:function listeners(){var _this=this;var rotateOn=false;this.engine.on('resize',function(event){_this.panorama.setOnCamera(_this.camera);});this.engine.on('rotstart',function(event){rotateOn=true;});this.engine.on('rotend',function(event){rotateOn=false;});this.engine.on('rotmove',function(event){if(rotateOn){_this.panorama.setOnCamera(_this.camera);}});$('#fade-button').click(function(){_this.fadeInOut();});var hammertime=new Hammer(document.getElementById('renderer'));hammertime.get('pinch').set({enable:true});hammertime.on('pinch',function(ev){var i=1;if(ev.scale>1.0){i=-1;}_this.camera.fov=_this.camera.fov+i;_this.camera.updateProjectionMatrix();});this.engine.on('mousewheel',function(event){var i=1;if(event.deltaY<0){i=-1;}var nextFov=_this.camera.fov+2*i;_this.camera.fov=nextFov<25?25:nextFov>125?125:nextFov;_this.camera.updateProjectionMatrix();});}},{key:'init',value:function init(){var _this2=this;var data={filename:'2',type:'jpg',path:'dist/images/2/'};var geoSphere=new THREE.SphereBufferGeometry(50,64,64);var matSphere=new THREE.MeshBasicMaterial({side:THREE.FrontSide,color:0xff0000,depthTest:false,depthWrite:false});var sph=new THREE.Mesh(geoSphere,matSphere);sph.position.x=175;this.scene.add(sph);this.panorama=new _pano.Pano(data,function(){console.log(_this2.panorama);_this2.activePanorama=_this2.panorama;_this2.scene.add(_this2.panorama.inner);setTimeout(function(){_this2.panorama.setOnCamera(_this2.camera);},500);});}},{key:'fadeInOut',value:function fadeInOut(){var _this3=this;var starta={x:this.sceneAlpha};this.sceneAlpha=1-this.sceneAlpha;var enda={x:this.sceneAlpha};var delay=2000;var fade=new TWEEN.Tween(starta).to(enda,delay).easing(TWEEN.Easing.Linear.None).onStart(function(){// console.log('START');
}).onUpdate(function(a){_this3.activePanorama.setAlpha(starta.x);// console.log('update', starta, a);
}).onComplete(function(){// console.log('COMPLETE');
});fade.start();}},{key:'setDoorAlpha',value:function setDoorAlpha(alpha){this.activePanorama.setDoorAlpha(alpha);}}]);return Viewer;}();

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports,"__esModule",{value:true});exports.Engine=undefined;var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();var _events=__webpack_require__(4);var _raycaster=__webpack_require__(5);function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}function _possibleConstructorReturn(self,call){if(!self){throw new ReferenceError("this hasn't been initialised - super() hasn't been called");}return call&&(typeof call==="object"||typeof call==="function")?call:self;}function _inherits(subClass,superClass){if(typeof superClass!=="function"&&superClass!==null){throw new TypeError("Super expression must either be null or a function, not "+typeof superClass);}subClass.prototype=Object.create(superClass&&superClass.prototype,{constructor:{value:subClass,enumerable:false,writable:true,configurable:true}});if(superClass)Object.setPrototypeOf?Object.setPrototypeOf(subClass,superClass):subClass.__proto__=superClass;}var Engine=exports.Engine=function(_Events){_inherits(Engine,_Events);function Engine(selector){_classCallCheck(this,Engine);// Init
var _this=_possibleConstructorReturn(this,(Engine.__proto__||Object.getPrototypeOf(Engine)).call(this));_this.container=document.querySelector(selector)||document.body;_this.width=_this.container.offsetWidth;_this.height=_this.container.offsetHeight;//this.height = this.container.scrollHeight;
// Scene
_this.scene=new THREE.Scene();_this.scene.name='Scene';// Renderer
_this.renderer=new THREE.WebGLRenderer({antialias:true,alpha:true});_this.renderer.setClearColor(0x000000,1.0);_this.renderer.setPixelRatio(window.devicePixelRatio);_this.renderer.setSize(_this.width,_this.height);_this.container.appendChild(_this.renderer.domElement);// Camera
_this.cameraP=new THREE.PerspectiveCamera(75,_this.width/_this.height,0.001,100000);_this.cameraP.name='CameraP';_this.cameraP.position.set(0,1,1.4);_this.cameraO=new THREE.OrthographicCamera(_this.width/-2,_this.width/2,_this.height/2,_this.height/-2,1,100000);_this.cameraO.name='CameraO';_this.cameraO.position.set(0,50,70);_this.camera=_this.cameraP;// Controls
_this.controls=new THREE.OrbitControls(_this.camera,_this.renderer.domElement);_this.controls.device=false;_this.controls.inverse=true;_this.controls.enablePan=false;_this.controls.enableZoom=false;_this.controls.enableDamping=false;_this.controls.target.set(0,0,0);_this.controls.update();_this.everyFrameStack=[];//Transform Controls
// this.transform = new THREE.TransformControls( this.camera, this.renderer.domElement );
// this.transform.addEventListener( 'change', this.render );
// this.scene.add(this.transform);
// Raycaster
_this.raycaster=new _raycaster.Raycaster(_this.camera,_this.renderer);_this.frame=0;// Function
_this.listen();_this.render();_this.debug();return _this;}_createClass(Engine,[{key:'listen',value:function listen(){var _this2=this;THREE.DefaultLoadingManager.onStart=function(){return _this2.emit('start');};THREE.DefaultLoadingManager.onProgress=function(item,loaded,total){return _this2.emit('progress',loaded/total*100,item,loaded,total);};THREE.DefaultLoadingManager.onLoad=function(){return _this2.emit('load');};THREE.DefaultLoadingManager.onError=function(){return _this2.emit('error');};this.renderer.domElement.addEventListener('click',function(event){return _this2.emit('click',event);},false);this.renderer.domElement.addEventListener('dblclick',function(event){return _this2.emit('dblclick',event);},false);this.renderer.domElement.addEventListener('contextmenu',function(event){return _this2.emit('contextmenu',event);},false);this.renderer.domElement.addEventListener('mousewheel',function(event){return _this2.emit('mousewheel',event);},false);this.renderer.domElement.addEventListener('mouseup',function(event){return _this2.emit('mouseup, pointerup, rotend',event);},false);this.renderer.domElement.addEventListener('touchend',function(event){return _this2.emit('rotend',event);},false);this.renderer.domElement.addEventListener('gesturestart',function(event){return _this2.emit('gesturestart',event);},false);this.renderer.domElement.addEventListener('gesturechange',function(event){return _this2.emit('gesturechange',event);},false);this.renderer.domElement.addEventListener('gestureend',function(event){return _this2.emit('gestureend',event);},false);this.renderer.domElement.addEventListener('mousedown',function(event){return _this2.emit('mousedown, pointerdown, rotstart',event);},false);this.renderer.domElement.addEventListener('touchstart',function(event){return _this2.emit('rotstart',event);},false);this.renderer.domElement.addEventListener('mousemove',function(event){return _this2.emit('mousemove, pointermove, rotmove',event);},false);this.renderer.domElement.addEventListener('mousewheel',function(event){return _this2.emit('mousewheel',event);},false);this.renderer.domElement.addEventListener('touchmove',function(event){return _this2.emit('rotmove',event);},false);window.addEventListener('keydown',function(event){return _this2.emit('keydown',event);},false);window.addEventListener('keyup',function(event){return _this2.emit('keyup',event);},false);window.addEventListener('orientationchange',function(){_this2.resize();_this2.emit('orientationchange');},false);window.addEventListener('resize',function(){_this2.resize();_this2.emit('resize');},false);}},{key:'resize',value:function resize(width,height){this.width=width||window.innerWidth;this.height=height||window.innerHeight;// this.width = width || this.container.offsetWidth;
// this.height = height || this.container.offsetHeight;
// console.log(this.container.offsetWidth, this.container.offsetHeight);
this.renderer.setPixelRatio(window.devicePixelRatio);this.camera.aspect=this.width/this.height;this.camera.updateProjectionMatrix();this.renderer.setSize(this.width,this.height);}},{key:'render',value:function render(){var _this3=this;this.renderer.clear();this.renderer.render(this.scene,this.camera);this.frame++;for(var i=0;i<this.everyFrameStack.length;i++){this.everyFrameStack[i](this.frame);}window.requestAnimationFrame(function(){TWEEN.update();_this3.controls.update();_this3.render();});}},{key:'everyFrame',value:function everyFrame(callback){if(callback){this.everyFrameStack.push(callback);}}},{key:'debug',value:function debug(){window.scene=this.scene;window.camera=this.camera;window.controls=this.controls;window.renderer=this.renderer;}}]);return Engine;}(_events.Events);

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports,"__esModule",{value:true});var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}var Events=exports.Events=function(){function Events(){_classCallCheck(this,Events);this.delegate={};}_createClass(Events,[{key:'on',value:function on(args,callback){var _this=this;args.split(',').forEach(function(name){name=name.trim();if(_this.delegate[name]){_this.delegate[name].push(callback);}else{_this.delegate[name]=[callback];}});return this;}},{key:'off',value:function off(args){var _this2=this;args.split(',').forEach(function(name){name=name.trim();delete _this2.delegate[name];});return this;}},{key:'emit',value:function emit(args){var _this3=this;for(var _len=arguments.length,params=Array(_len>1?_len-1:0),_key=1;_key<_len;_key++){params[_key-1]=arguments[_key];}args.split(',').forEach(function(name){name=name.trim();if(_this3.delegate[name]){_this3.delegate[name].forEach(function(event){event.apply(undefined,params);});}});return this;}}]);return Events;}();

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports,"__esModule",{value:true});var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}var Raycaster=exports.Raycaster=function(){function Raycaster(camera,renderer){_classCallCheck(this,Raycaster);this.camera=camera;this.renderer=renderer;this.raycaster=new THREE.Raycaster();}_createClass(Raycaster,[{key:'hit',value:function hit(event,object,done){var recursive=arguments.length>3&&arguments[3]!==undefined?arguments[3]:true;var pointer=event.changedTouches?event.changedTouches[0]:event;var rect=this.renderer.domElement.getBoundingClientRect();var mouse=new THREE.Vector2((pointer.clientX-rect.left)/rect.width*2-1,-((pointer.clientY-rect.top)/rect.height)*2+1);var origin=new THREE.Vector3();var direction=new THREE.Vector3();var intersects=null;this.camera.updateMatrixWorld();if(this.camera instanceof THREE.PerspectiveCamera||this.camera.inPerspectiveMode){origin.setFromMatrixPosition(this.camera.matrixWorld);direction.set(mouse.x,mouse.y,0.5).unproject(this.camera).sub(origin).normalize();}else if(this.camera instanceof THREE.OrthographicCamera||this.camera.inOrthographicMode){origin.set(mouse.x,mouse.y,(this.camera.near+this.camera.far)/(this.camera.near-this.camera.far)).unproject(this.camera);// set origin in plane of camera
direction.set(0,0,-1).transformDirection(this.camera.matrixWorld);}else{console.warn('Unsupported camera');}this.raycaster.set(origin,direction);if(Array.isArray(object)){intersects=this.raycaster.intersectObjects(object,recursive);}else{intersects=this.raycaster.intersectObject(object,recursive);}done(intersects);}}]);return Raycaster;}();

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports,"__esModule",{value:true});var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}var Utils=exports.Utils=function(){function Utils(){_classCallCheck(this,Utils);}_createClass(Utils,null,[{key:"getNormalBy3Points",value:function getNormalBy3Points(points){var tempGeometry=new THREE.Geometry();tempGeometry.vertices.push(points[0].clone());tempGeometry.vertices.push(points[1].clone());tempGeometry.vertices.push(points[2].clone());var normal=new THREE.Vector3(0,1,0);//optional
var face=new THREE.Face3(0,1,2,normal);tempGeometry.faces.push(face);tempGeometry.computeFaceNormals();return tempGeometry.faces[0].normal.clone();}}]);return Utils;}();

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports,"__esModule",{value:true});var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}var Pano=exports.Pano=function(){function Pano(data,onLoad){var _this=this;_classCallCheck(this,Pano);this.data=data||{};this.sections=8;this.steps=8;this.baseSize=48;this.activeOpacity=1;this.activeDoorOpacity=0.5;this.piLength=Math.PI*2/this.sections;this.thetaLength=Math.PI/this.sections;this.sectNums={'0':1,'1':0,'2':7,'3':6,'4':5,'5':4,'6':3,'7':2,'8':1};this.stepsNums={'0':7,'1':6,'2':5,'3':4,'4':3,'5':2,'6':1,'7':0,'8':0};//
this.images={};for(var i=0;i<this.sections;i++){this.images[i]=[];}//
this._onLoad=function(){};this.inner=new THREE.Group();this.inner.name='Inner';this.rotx=0.0;this.inner.rotation.x=this.rotx;this.material=new THREE.MeshBasicMaterial({side:THREE.DoubleSide});this.materialSection=new THREE.MeshBasicMaterial({side:THREE.BackSide});// texture.magFilter = THREE.LinearFilter;
// texture.minFilter = THREE.LinearFilter;
var imagesToLoad=0;var nextIsLoaded=function nextIsLoaded(){imagesToLoad--;//console.log( 'imgesToLoad', imagesToLoad );
if(imagesToLoad===0){_this._onLoad();}};for(var _i=0;_i<this.sections;_i++){var piStart=_i*this.piLength;for(var j=0;j<this.steps;j++){imagesToLoad++;var thetaStart=j*this.thetaLength;var src=this.data.path+'/vlowres/'+this.data.filename+'-'+j+'-'+_i+'.jpg';// let text = new THREE.Texture(src);
// let text = THREE.ImageUtils.loadTexture(src);
// let text = new THREE.TextureLoader().load( src, ( txtr ) => {
//     nextIsLoaded();
// } );
// text.minFilter = THREE.LinearMipMapLinearFilter;
// text.magFilter = THREE.LinearFilter;
var nextMaterial=new THREE.MeshBasicMaterial({depthTest:false,depthWrite:false,side:THREE.FrontSide//color: new THREE.Color( Math.random(), Math.random(), Math.random() ),
//visible: false,
// transparent: true,
// opacity: 1
});//nextMaterial.blending = THREE.CustomBlending;
//nextMaterial.blendEquation = THREE.MinEquation; //default
//nextMaterial.blendSrc = THREE.DstAlphaFactor; //default
//nextMaterial.blendSrcAlpha = THREE.OneFactor; //default
//nextMaterial.blendDst = THREE.SrcAlphaSaturateFactor; //default
var section=new THREE.Mesh(new THREE.SphereBufferGeometry(100,this.baseSize/this.sections,this.baseSize/this.steps,piStart,this.piLength,thetaStart,this.thetaLength));section.orderIndex=_i;section.stepIndex=j;section.geometry.scale(-1,1,1);this.inner.add(section);}//this.inner.scale.set( 15, 15, 15 );
}this.onLoad(onLoad);this.loadShader();// this.loadTexture();
}_createClass(Pano,[{key:'onLoad',value:function onLoad(func){if(typeof func=='function'){this._onLoad=func;}}},{key:'setOnCamera',value:function setOnCamera(camera){var _this2=this;camera._frustumUpdate();this.inner.children.forEach(function(child){var updateTextureQuality=function updateTextureQuality(){var src=_this2.data.path+_this2.data.filename+'-'+child.stepIndex+'-'+child.orderIndex+'.jpg';new THREE.TextureLoader().load(src,function(txtr){var alpha=_this2.data.path+'alpha/'+_this2.data.filename+'-'+child.stepIndex+'-'+child.orderIndex+'.jpg';new THREE.TextureLoader().load(alpha,function(am){var mat=_this2.shaderHighRes.clone();mat.uniforms.map.value=txtr;mat.uniforms.map.value.needsUpdate=true;mat.uniforms.doorIntensity.value=_this2.activeDoorOpacity;mat.uniforms.intensity.value=_this2.activeOpacity;child.material=mat;// child.material = new THREE.MeshBasicMaterial({
//     depthTest: false,
//     depthWrite: true,
//     side: THREE.FrontSide,
//     map: txtr,
//     alphaMap: am,
//     transparent: true,
//     opacity: this.activeOpacity || 1
// });
// child.material.map.needsUpdate = true;
});});child.done=true;};// Check by hardcode
var checkByFrustum=true;if(!child.done&&camera._frustum.intersectsObject(child)){updateTextureQuality();}});}},{key:'highRes',value:function highRes(){this.shaderHighRes_old=new THREE.ShaderMaterial({uniforms:{map:{type:'t',value:null},alpha:{type:'t',value:null},intensity:{type:'f',value:1.0},doorIntensity:{type:'f',value:1.0}},vertexShader:'\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							void main () {\n\
								vec4 p = vec4 (position, 1.0);\n\
								vUv = uv;\n\
								worldPosition = (modelMatrix * p).xyz;\n\
								gl_Position = projectionMatrix * modelViewMatrix * p;\n\
							}',fragmentShader:'\
							uniform sampler2D map;\n\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							uniform float intensity;\n\
							uniform float doorIntensity;\n\
							uniform sampler2D alpha;\n\
							void main () {\n\
								gl_FragColor = texture2D(map, vUv);\n\
							}'});this.shaderHighRes=new THREE.ShaderMaterial({uniforms:{map:{type:'t',value:null},intensity:{type:'f',value:1.0},doorIntensity:{type:'f',value:this.activeDoorOpacity},point_l:{type:'v3',value:new THREE.Vector3()},point_o:{type:'v3',value:new THREE.Vector3()},point_e:{type:'v3',value:new THREE.Vector3()},point_u:{type:'v3',value:new THREE.Vector3()},plane:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane1:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane2:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane3:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane4:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane5:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane6:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},rotationY:{type:'f',value:0},rotationX:{type:'f',value:0},placement:{type:'v3',value:new THREE.Vector3()}},vertexShader:'\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							void main () {\n\
								vec4 p = vec4 (position, 1.0);\n\
								vUv = uv;\n\
								worldPosition = (modelMatrix * p).xyz;\n\
								gl_Position = projectionMatrix * modelViewMatrix * p;\n\
								//gl_Position = vec4(uv * 2.0 - vec2(1.0), 0.0, 1.0);\n\
							}',fragmentShader:'\
							uniform sampler2D map;\n\
							uniform vec3 placement;\n\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							uniform vec3 point_l;\n\
							uniform vec3 point_o;\n\
							uniform vec3 point_e;\n\
							uniform vec3 point_u;\n\
							uniform vec4 plane;\n\
							uniform vec4 plane1;\n\
							uniform vec4 plane2;\n\
							uniform vec4 plane3;\n\
							uniform vec4 plane4;\n\
							uniform vec4 plane5;\n\
							uniform vec4 plane6;\n\
							uniform float radius;\n\
							uniform float intensity;\n\
							uniform float doorIntensity;\n\
							uniform float rotationY;\n\
							uniform float rotationX;\n\
							uniform sampler2D alpha;\n\
							void main () {\n\
								vec3 R = worldPosition - placement;\n\
								float oldY = R.y;\n\
								float oldZ = R.z;\n\
								R.y = oldY * cos(rotationX) - oldZ * sin(rotationX);\n\
                                R.z = oldY * sin(rotationX) + oldZ * cos(rotationX);\n\
                                float pp = 1.0;\n\
								float r = length (R);\n\
								float theta = acos(-R.y / r);\n\
								float phi = atan(R.x, -R.z);\n\
								float roty =  (0.75 + phi  / 6.2831853072) + ( rotationY  / 6.2831853072);\n\
								vec3 normR = normalize(R);\n\
								vec3 normL = normalize(point_l);\n\
								if(distance(normR, normL) < 1.414) {\n\
                                    float t = -(plane.w)/(plane.x*R.x + plane.y*R.y + plane.z*R.z);\n\
                                    if(t != 0.0) {\n\
                                        float xo = R.x*t;\n\
                                        float yo = R.y*t;\n\
                                        float zo = R.z*t;\n\
                                        vec3 P = vec3(xo,yo,zo);\n\
                                        if(dot((P - point_o),vec3(plane3.x, plane3.y, plane3.z)) > 0.0 && dot((P - point_l),vec3(plane2.x, plane2.y, plane2.z)) > 0.0 && dot((P - point_l),vec3(plane1.x, plane1.y, plane1.z)) < 0.0 && dot((P - point_o),vec3(plane4.x, plane4.y, plane4.z)) < 0.0){\n\
                                            pp = doorIntensity;\n\
                                        }\n\
                                    }\n\
                                }\n\
                                if(roty > 1.0) {\n\
                                    roty = roty - 1.0;\n\
                                }\n\
                                if(roty < .0) {\n\
                                    roty = roty + 1.0;\n\
                                }\n\
                                vec4 test = texture2D(map, vUv);\n\
								gl_FragColor = vec4(vec3(test),pp * intensity);\n\
								\n\
							}'});this.shaderHighRes.side=THREE.FrontSide;this.shaderHighRes.transparent=true;}},{key:'loadShader',value:function loadShader(){var _this3=this;this.highRes();this.shader_old=new THREE.ShaderMaterial({uniforms:{map:{type:'t',value:null},alpha:{type:'t',value:null},intensity:{type:'f',value:1.0},doorIntensity:{type:'f',value:this.activeDoorOpacity},rotationY:{type:'f',value:0},rotationX:{type:'f',value:0},placement:{type:'v3',value:new THREE.Vector3()}},vertexShader:'\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							void main () {\n\
								vec4 p = vec4 (position, 1.0);\n\
								worldPosition = (modelMatrix * p).xyz;\n\
								gl_Position = projectionMatrix * modelViewMatrix * p;\n\
								//gl_Position = vec4(uv * 2.0 - vec2(1.0), 0.0, 1.0);\n\
							}',fragmentShader:'\
							uniform sampler2D map;\n\
							uniform vec3 placement;\n\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							uniform float radius;\n\
							uniform float intensity;\n\
							uniform float doorIntensity;\n\
							uniform float rotationY;\n\
							uniform float rotationX;\n\
							uniform sampler2D alpha;\n\
							void main () {\n\
								vec3 R = worldPosition - placement;\n\
								float oldY = R.y;\n\
								float oldZ = R.z;\n\
								R.y = oldY * cos(rotationX) - oldZ * sin(rotationX);\n\
                                R.z = oldY * sin(rotationX) + oldZ * cos(rotationX);\n\
								float r = length (R);\n\
								float theta = acos(-R.y / r);\n\
								float phi = atan(R.x, -R.z);\n\
								float roty =  (0.75 + phi  / 6.2831853072) + ( rotationY  / 6.2831853072);\n\
                                if(roty > 1.0) {\n\
                                    roty = roty - 1.0;\n\
                                }\n\
                                if(roty < .0) {\n\
                                    roty = roty + 1.0;\n\
                                }\n\
                                vec4 test = texture2D(map, vec2 (\n\
                                    roty,\n\
                                    theta / 3.1415926536\n\
                                ));\n\
                                vec4 am = texture2D(alpha, vec2 (\n\
                                    roty,\n\
                                    theta / 3.1415926536\n\
                                ));\n\
                                float ap = (am.g + am.r + am.b)/3.0;\n\
								gl_FragColor = vec4(vec3(test),ap);\n\
								\n\
							}'});this.shader=new THREE.ShaderMaterial({uniforms:{map:{type:'t',value:null},alpha:{type:'t',value:null},intensity:{type:'f',value:1.0},doorIntensity:{type:'f',value:this.activeDoorOpacity},point_l:{type:'v3',value:new THREE.Vector3()},point_o:{type:'v3',value:new THREE.Vector3()},point_e:{type:'v3',value:new THREE.Vector3()},point_u:{type:'v3',value:new THREE.Vector3()},plane:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane1:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane2:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane3:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane4:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane5:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},plane6:{type:'v4',value:new THREE.Vector4(1.0,1.0,1.0,0.0)},rotationY:{type:'f',value:0},rotationX:{type:'f',value:0},placement:{type:'v3',value:new THREE.Vector3()}},vertexShader:'\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							void main () {\n\
								vec4 p = vec4 (position, 1.0);\n\
								worldPosition = (modelMatrix * p).xyz;\n\
								gl_Position = projectionMatrix * modelViewMatrix * p;\n\
								//gl_Position = vec4(uv * 2.0 - vec2(1.0), 0.0, 1.0);\n\
							}',fragmentShader:'\
							uniform sampler2D map;\n\
							uniform vec3 placement;\n\
							varying vec3 worldPosition;\n\
							varying vec2 vUv;\n\
							uniform vec3 point_l;\n\
							uniform vec3 point_o;\n\
							uniform vec3 point_e;\n\
							uniform vec3 point_u;\n\
							uniform vec4 plane;\n\
							uniform vec4 plane1;\n\
							uniform vec4 plane2;\n\
							uniform vec4 plane3;\n\
							uniform vec4 plane4;\n\
							uniform vec4 plane5;\n\
							uniform vec4 plane6;\n\
							uniform float radius;\n\
							uniform float intensity;\n\
							uniform float doorIntensity;\n\
							uniform float rotationY;\n\
							uniform float rotationX;\n\
							uniform sampler2D alpha;\n\
							void main () {\n\
								vec3 R = worldPosition - placement;\n\
								float oldY = R.y;\n\
								float oldZ = R.z;\n\
								R.y = oldY * cos(rotationX) - oldZ * sin(rotationX);\n\
                                R.z = oldY * sin(rotationX) + oldZ * cos(rotationX);\n\
                                float pp = 1.0;\n\
								float r = length (R);\n\
								float theta = acos(-R.y / r);\n\
								float phi = atan(R.x, -R.z);\n\
								float roty =  (0.75 + phi  / 6.2831853072) + ( rotationY  / 6.2831853072);\n\
                                vec3 normR = normalize(R);\n\
								vec3 normL = normalize(point_l);\n\
								if(distance(normR, normL) < 1.414) {\n\
                                    float t = -(plane.w)/(plane.x*R.x + plane.y*R.y + plane.z*R.z);\n\
                                    if(t != 0.0) {\n\
                                        float xo = R.x*t;\n\
                                        float yo = R.y*t;\n\
                                        float zo = R.z*t;\n\
                                        vec3 P = vec3(xo,yo,zo);\n\
                                        if(dot((P - point_o),vec3(plane3.x, plane3.y, plane3.z)) > 0.0 && dot((P - point_l),vec3(plane2.x, plane2.y, plane2.z)) > 0.0 && dot((P - point_l),vec3(plane1.x, plane1.y, plane1.z)) < 0.0 && dot((P - point_o),vec3(plane4.x, plane4.y, plane4.z)) < 0.0){\n\
                                            pp = doorIntensity;\n\
                                        }\n\
                                    }\n\
                                }\n\
                                if(roty > 1.0) {\n\
                                    roty = roty - 1.0;\n\
                                }\n\
                                if(roty < .0) {\n\
                                    roty = roty + 1.0;\n\
                                }\n\
                                vec4 test = texture2D(map, vec2 (\n\
                                    roty,\n\
                                    theta / 3.1415926536\n\
                                ));\n\
                                vec4 am = texture2D(alpha, vec2 (\n\
                                    roty,\n\
                                    theta / 3.1415926536\n\
                                ));\n\
								gl_FragColor = vec4(vec3(test),pp * intensity);\n\
								\n\
							}'});var l=new THREE.Vector3(25,15,-25);var m=new THREE.Vector3(25,15,5);var n=new THREE.Vector3(25,-30,-25);var o=new THREE.Vector3(25,-30,5);var plane=new THREE.Plane();plane.setFromCoplanarPoints(l,m,n);var f=new THREE.Vector3(l.x+plane.normal.x,l.y+plane.normal.y,l.z+plane.normal.z);var e=new THREE.Vector3(m.x+plane.normal.x,m.y+plane.normal.y,m.z+plane.normal.z);var h=new THREE.Vector3(o.x+plane.normal.x,o.y+plane.normal.y,o.z+plane.normal.z);var u=new THREE.Vector3(l.x-plane.normal.x,l.y-plane.normal.y,l.z-plane.normal.z);var r=new THREE.Vector3(m.x-plane.normal.x,m.y-plane.normal.y,m.z-plane.normal.z);var t=new THREE.Vector3(o.x-plane.normal.x,o.y-plane.normal.y,o.z-plane.normal.z);var plane1=new THREE.Plane();plane1.setFromCoplanarPoints(l,m,f);//top
var plane2=new THREE.Plane();plane2.setFromCoplanarPoints(l,f,n);//right
plane2.normal.z*=-1;var plane3=new THREE.Plane();plane3.setFromCoplanarPoints(o,m,e);//left
var plane4=new THREE.Plane();plane4.setFromCoplanarPoints(o,h,n);//bottom
plane4.normal.y*=-1;var plane5=new THREE.Plane();plane5.setFromCoplanarPoints(f,e,h);//front
var plane6=new THREE.Plane();plane6.setFromCoplanarPoints(u,r,t);//back
plane6.normal.x*=-1;this.shader.uniforms.plane.value=new THREE.Vector4(plane.normal.x,plane.normal.y,plane.normal.z,plane.constant);this.shader.uniforms.plane1.value=new THREE.Vector4(plane1.normal.x,plane1.normal.y,plane1.normal.z,plane1.constant);this.shader.uniforms.plane2.value=new THREE.Vector4(plane2.normal.x,plane2.normal.y,plane2.normal.z,plane2.constant);this.shader.uniforms.plane3.value=new THREE.Vector4(plane3.normal.x,plane3.normal.y,plane3.normal.z,plane3.constant);this.shader.uniforms.plane4.value=new THREE.Vector4(plane4.normal.x,plane4.normal.y,plane4.normal.z,plane4.constant);this.shader.uniforms.plane5.value=new THREE.Vector4(plane5.normal.x,plane5.normal.y,plane5.normal.z,plane5.constant);this.shader.uniforms.plane6.value=new THREE.Vector4(plane6.normal.x,plane6.normal.y,plane6.normal.z,plane6.constant);this.shader.uniforms.point_e.value=e;this.shader.uniforms.point_l.value=l;this.shader.uniforms.point_o.value=o;this.shader.uniforms.point_u.value=u;this.shader.uniforms.rotationX.value=-this.rotx;this.shader.side=THREE.FrontSide;this.shader.transparent=true;this.shaderHighRes.uniforms.plane.value=new THREE.Vector4(plane.normal.x,plane.normal.y,plane.normal.z,plane.constant);this.shaderHighRes.uniforms.plane1.value=new THREE.Vector4(plane1.normal.x,plane1.normal.y,plane1.normal.z,plane1.constant);this.shaderHighRes.uniforms.plane2.value=new THREE.Vector4(plane2.normal.x,plane2.normal.y,plane2.normal.z,plane2.constant);this.shaderHighRes.uniforms.plane3.value=new THREE.Vector4(plane3.normal.x,plane3.normal.y,plane3.normal.z,plane3.constant);this.shaderHighRes.uniforms.plane4.value=new THREE.Vector4(plane4.normal.x,plane4.normal.y,plane4.normal.z,plane4.constant);this.shaderHighRes.uniforms.plane5.value=new THREE.Vector4(plane5.normal.x,plane5.normal.y,plane5.normal.z,plane5.constant);this.shaderHighRes.uniforms.plane6.value=new THREE.Vector4(plane6.normal.x,plane6.normal.y,plane6.normal.z,plane6.constant);this.shaderHighRes.uniforms.point_e.value=e;this.shaderHighRes.uniforms.point_l.value=l;this.shaderHighRes.uniforms.point_o.value=o;this.shaderHighRes.uniforms.point_u.value=u;this.shaderHighRes.uniforms.rotationX.value=-this.rotx;var image=new Image();image.onload=function(){var texture=new THREE.Texture(image);texture.magFilter=THREE.LinearFilter;texture.minFilter=THREE.LinearFilter;_this3.shader.uniforms.map.value=texture;_this3.shader.uniforms.map.value.needsUpdate=true;_this3.shader.uniforms.placement.value={x:0,y:0,z:0};var map=new Image();map.onload=function(){var amap=new THREE.Texture(map);amap.magFilter=THREE.LinearFilter;amap.minFilter=THREE.LinearFilter;_this3.shader.uniforms.alpha.value=amap;_this3.shader.uniforms.alpha.value.needsUpdate=true;_this3.inner.children.forEach(function(child){child.material=_this3.shader;if(child.stepIndex===3&&child.orderIndex%2===0){child.done=true;}});_this3._onLoad();};map.crossOrigin='anonymous';map.src=_this3.data.path+'/'+_this3.data.filename+'_alpha.jpg';};image.crossOrigin='anonymous';var src=this.data.path+'/vlowres/'+this.data.filename+'.jpg';image.src=src;}},{key:'loadTexture',value:function loadTexture(){var _this4=this;var _loop=function _loop(i){_this4.images[i]=[];var _loop2=function _loop2(j){_this4.images[i][j]=new Image();_this4.images[i][j].onload=function(){_this4.inner.children.forEach(function(child){if(child.orderIndex===i&&child.stepIndex===j){child.material.map.image=_this4.images[child.orderIndex][child.stepIndex];child.material.map.needsUpdate=true;}});};// let src = this.data.path + '/lowres/' + this.data.filename + '-' + j + '-' + i + '.jpg';
// let src = this.data.path + '/midres/' + this.data.filename + '-' + j + '-' + i + '.jpg';
// let src = this.data.path + this.data.filename + '-' + j + '-' + i + '.jpg';
// this.images[i][j].src = src;
};for(var j=0;j<_this4.steps;j++){_loop2(j);}};for(var i=0;i<this.sections;i++){_loop(i);}// this.inner.children.forEach(function(child) {
//     child.material.needsUpdate = true;
// });
// let image = new Image();
//
// image.onload = () => {
//     this.mesh.material.map = new THREE.Texture(image);
//     this.mesh.material.map.needsUpdate = true;
//     this.mesh.material.needsUpdate = true;
// };
//
// image.crossOrigin = 'anonymous';
//
// image.src = this.data.src;
}},{key:'setAlpha',value:function setAlpha(alpha){this.activeOpacity=alpha;this.shader.uniforms.intensity.value=alpha;this.inner.children.forEach(function(child){if(child.done){child.material.uniforms.intensity.value=alpha;}});}},{key:'setDoorAlpha',value:function setDoorAlpha(alpha){this.activeDoorOpacity=alpha;this.shader.uniforms.doorIntensity.value=alpha;// this.shaderHighRes.uniforms.doorIntensity.value = alpha;
this.inner.children.forEach(function(child){if(child.done){child.material.uniforms.doorIntensity.value=alpha;}});}}]);return Pano;}();

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports,"__esModule",{value:true});var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||false;descriptor.configurable=true;if("value"in descriptor)descriptor.writable=true;Object.defineProperty(target,descriptor.key,descriptor);}}return function(Constructor,protoProps,staticProps){if(protoProps)defineProperties(Constructor.prototype,protoProps);if(staticProps)defineProperties(Constructor,staticProps);return Constructor;};}();function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor)){throw new TypeError("Cannot call a class as a function");}}var Door=exports.Door=function(){function Door(doorPoints){_classCallCheck(this,Door);//this.width = width;
//this.height = height;
this.lineWidth=0.2;this.mesh=new THREE.Object3D();this.position=this.mesh.position;this.rotation=this.mesh.rotation;this.TextureLoader=new THREE.TextureLoader();this.TextureLoader.crossOrigin='Anonymous';this.doorPoints=[doorPoints[0],doorPoints[1],doorPoints[2],doorPoints[3],doorPoints[0]];this.doorElementsConfig=[{name:'line1',category:'line',from:doorPoints[3],to:doorPoints[0]},{name:'line2',category:'line',from:doorPoints[0],to:doorPoints[1]},{name:'line3',category:'line',from:doorPoints[1],to:doorPoints[2]},{name:'line4',category:'line',from:doorPoints[2],to:doorPoints[3]//,{
//    name: 'line5',
//    category: 'line',
//    from: new THREE.Vector3( -(this.width/2), -(this.height/2), 0  ),
//    to: new THREE.Vector3( -(this.width/2), 0, 0  )
//}
},{name:'corner1',category:'corner',rotation:0,position:doorPoints[0]},{name:'corner2',category:'corner',rotation:-Math.PI/2*1,position:doorPoints[1]},{name:'corner3',category:'corner',rotation:-Math.PI/2*2,position:doorPoints[2]},{name:'corner4',category:'corner',rotation:-Math.PI/2*3,position:doorPoints[3]}];this.init();}_createClass(Door,[{key:'loadTextures',value:function loadTextures(){this.lineCornerTexture=this.TextureLoader.load('dist/resources/texture/glow.texture.1.png');this.lineCornerTexture.minFilter=THREE.LinearMipMapLinearFilter;this.lineCornerTexture.magFilter=THREE.LinearFilter;this.lineTexture=this.TextureLoader.load('dist/resources/texture/glow.texture.2.png');this.lineTexture.minFilter=THREE.LinearMipMapLinearFilter;this.lineTexture.magFilter=THREE.LinearFilter;this.lineTexture.wrapS=this.lineTexture.wrapT=THREE.RepeatWrapping;this.lineTexture.offset.set(0,0);this.lineTexture.repeat.set(1,2);}},{key:'initGeometryes',value:function initGeometryes(){this.scalebleElements={};for(var i=0;i<this.doorElementsConfig.length;i++){var nextConfig=this.doorElementsConfig[i];var depthTest=false;var depthWrite=false;var BlendingParameter=1;//let BlendingParameter = THREE.NormalBlending;
if(nextConfig.category==='line'){var length=nextConfig.from.distanceTo(nextConfig.to);var position=nextConfig.from.clone().lerp(nextConfig.to,0.5);var geom=new THREE.PlaneGeometry(this.lineWidth,length,1,1);var mater=new THREE.MeshBasicMaterial({depthTest:depthTest,depthWrite:depthWrite,side:THREE.DoubleSide,transparent:true,blending:BlendingParameter,map:this.lineTexture});//console.log( mater );
var nextPlane=new THREE.Mesh(geom,mater);nextPlane.name='line';nextPlane.position.copy(position);//nextPlane.scale.set( 0.3, 1, 1 );
if(nextConfig.from.y===nextConfig.to.y){nextPlane.rotation.set(0,0,-Math.PI/2);}nextPlane.config=nextConfig;this.scalebleElements[nextConfig.name]=nextPlane;this.mesh.add(nextPlane);}else if(nextConfig.category==='corner'){var geom2=new THREE.PlaneGeometry(this.lineWidth,this.lineWidth,1,1);var mater2=new THREE.MeshBasicMaterial({depthTest:depthTest,depthWrite:depthWrite,side:THREE.DoubleSide,blending:BlendingParameter,transparent:true,map:this.lineCornerTexture});//console.log( mater2 );
var nextPlane2=new THREE.Mesh(geom2,mater2);nextPlane2.name='corner';nextPlane2.position.copy(nextConfig.position);nextPlane2.rotation.set(0,0,nextConfig.rotation);nextPlane2.config=nextConfig;this.scalebleElements[nextConfig.name]=nextPlane2;this.mesh.add(nextPlane2);}}}},{key:'prepareForTween',value:function prepareForTween(){for(var key in this.scalebleElements){this.scalebleElements[key].visible=false;}}},{key:'tweenScenaryStart',value:function tweenScenaryStart(){var _this=this;this.prepareForTween();var _tweens=this.tweens=[];var tryStartNext=function tryStartNext(i){if(_tweens[i+1]){_tweens[i+1].start();}};var scalebleElements=this.scalebleElements;this.tweenScenary=[{d:75,S:function S(){scalebleElements.line1.visible=true;},U:function U(duration){_this.scalebleElements.line1.scale.set(1,duration,1);var nextPosition=_this.scalebleElements.line1.config.from.clone().lerp(_this.scalebleElements.line1.config.to,0.5*duration);_this.scalebleElements.line1.position.copy(nextPosition);},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:1,S:function S(){scalebleElements.corner1.visible=true;},U:function U(duration){},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:75,S:function S(){scalebleElements.line2.visible=true;},U:function U(duration){_this.scalebleElements.line2.scale.set(1,duration,1);var nextPosition=_this.scalebleElements.line2.config.from.clone().lerp(_this.scalebleElements.line2.config.to,0.5*duration);_this.scalebleElements.line2.position.copy(nextPosition);},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:1,S:function S(){scalebleElements.corner2.visible=true;},U:function U(duration){},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:75,S:function S(){scalebleElements.line3.visible=true;},U:function U(duration){_this.scalebleElements.line3.scale.set(1,duration,1);var nextPosition=_this.scalebleElements.line3.config.from.clone().lerp(_this.scalebleElements.line3.config.to,0.5*duration);_this.scalebleElements.line3.position.copy(nextPosition);},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:1,S:function S(){scalebleElements.corner3.visible=true;},U:function U(duration){},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:75,S:function S(){scalebleElements.line4.visible=true;},U:function U(duration){_this.scalebleElements.line4.scale.set(1,duration,1);var nextPosition=_this.scalebleElements.line4.config.from.clone().lerp(_this.scalebleElements.line4.config.to,0.5*duration);_this.scalebleElements.line4.position.copy(nextPosition);},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:1,S:function S(){scalebleElements.corner4.visible=true;},U:function U(duration){},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},//{
//    d: 75,
//    S: function(){ scalebleElements.line5.visible = true; },
//    U: ( duration ) => {
//        this.scalebleElements.line5.scale.set( 1, duration, 1 );
//        let nextPosition = this.scalebleElements.line5.config.from.clone().lerp( this.scalebleElements.line5.config.to, 0.5 * duration );
//        this.scalebleElements.line5.position.copy( nextPosition );
//    },
//    C: function (){ if( this.i < _tweens.length-1 ){ tryStartNext( this.i ); }  }
//},
{d:1000,S:function S(){},U:function U(duration){var _alpha=duration;for(var key in _this.scalebleElements){if(_this.scalebleElements[key].name==='corner'){_this.scalebleElements[key].scale.set(1+_alpha*8,1+_alpha*8,1);}else{_this.scalebleElements[key].scale.set(1+_alpha*8,1,1);}}},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:100,S:function S(){},U:function U(duration){},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:2000,S:function S(){},U:function U(duration){var _alpha=1-duration;for(var key in _this.scalebleElements){if(_this.scalebleElements[key].name==='corner'){_this.scalebleElements[key].scale.set(1+_alpha*8,1+_alpha*8,1);}else{_this.scalebleElements[key].scale.set(1+_alpha*8,1,1);}}},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:300,S:function S(){},U:function U(duration){},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:2000,S:function S(){},U:function U(duration){var _alpha=duration;for(var key in _this.scalebleElements){if(_this.scalebleElements[key].name==='corner'){_this.scalebleElements[key].scale.set(1+_alpha*8,1+_alpha*8,1);}else{_this.scalebleElements[key].scale.set(1+_alpha*8,1,1);}}},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:2000,S:function S(){},U:function U(duration){},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:1000,S:function S(){},U:function U(duration){var _alpha=1-duration;for(var key in _this.scalebleElements){if(_this.scalebleElements[key].name==='corner'){_this.scalebleElements[key].scale.set(0.000001+_alpha*9,0.000001+_alpha*9,1);}else{_this.scalebleElements[key].scale.set(0.000001+_alpha*9,1,1);}}},C:function C(){if(this.i<_tweens.length-1){tryStartNext(this.i);}}},{d:2000,S:function S(){},U:function U(duration){},C:function C(){tryStartNext(-1);}}];// Package tweens to stack
if(_tweens.length==0){for(var i=0;i<this.tweenScenary.length;i++){var _data={a:0,i:i};var nextScenary=this.tweenScenary[i];var nextTween=new TWEEN.Tween(_data).to({a:1},nextScenary.d).easing(TWEEN.Easing.Linear.None).onStart(nextScenary.S).onUpdate(nextScenary.U).onComplete(nextScenary.C);_tweens.push(nextTween);}}// init animation
tryStartNext(-1);}},{key:'init',value:function init(){this.loadTextures();this.initGeometryes();this.prepareForTween();this.tweenScenaryStart();}},{key:'animate',value:function animate(currentFrame){//let _alpha = Math.abs( Math.sin( currentFrame/75 ) );
}},{key:'initCanvas',value:function initCanvas(){this.updateTexture();}},{key:'drawCanvas',value:function drawCanvas(){var lineWidth=arguments.length>0&&arguments[0]!==undefined?arguments[0]:3;var blur=arguments.length>1&&arguments[1]!==undefined?arguments[1]:3;this.updateTexture();}},{key:'updateTexture',value:function updateTexture(){this.texture.needsUpdate=true;}},{key:'setOnCamera',value:function setOnCamera(){}},{key:'loadTexture',value:function loadTexture(){}}]);return Door;}();

/***/ })
/******/ ]);
//# sourceMappingURL=viewer.js.map