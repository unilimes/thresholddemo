const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

function getDevTool() {
    if (process.env.NODE_ENV !== 'production') {
        return 'source-map';
    }
    return false;
}

const isProduction = (process.env.NODE_ENV === 'production');

const extractSass = new ExtractTextPlugin({
    filename: 'dist/style.min.css'
});

function devOrProd(development, production) {
    return isProduction ? production : development;
}

module.exports = {
    entry: {
        viewer: path.join(__dirname, '/source/main.js')
    },
    output: {
        path: path.join(__dirname, '/public'),
        filename: 'dist/[name].js'
    },
    devtool: getDevTool(),
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /(node_modules|bower_components)/,
                use: isProduction ? [
                    {
                        loader: 'babel-loader',
                        options: {
                            compact: true,
                            babelrc: false,
                            presets: ['env', 'stage-2']
                        }
                    }
                ] : [
                    {
                        loader: 'babel-loader',
                        options: {
                            compact: true,
                            babelrc: false,
                            presets: ['env', 'stage-2']
                        }
                    }
                    ,
                    {
                        loader: 'eslint-loader',
                        options: {
                            fix: true,
                            cache: true
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            minimize: true
                        }
                    }, {
                        loader: 'sass-loader'
                    }]
                })
            }
        ]
    },
    plugins: [
        extractSass
        // ,
        // new HtmlWebpackPlugin({
        //     template: 'source/index.html',
        //     filename: 'index.html',
        //     inject: 'body',
        //     defer: ['viewer'],
        //     hash: true
        // }),
        // new ScriptExtHtmlWebpackPlugin({
        //     defaultAttribute: 'defer'
        // })
    ]
};